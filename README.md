<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>



## Requisitos
Para prosseguir com a instalação é necessário possuir as seguintes extensões/softwares/versões instaladas:

PHP >= 7.3.2

MySQL >= 5.0.12

Composer

JSON PHP Extension

## Configurações HTTP
O servidor HTTP deverá apontar para a pasta public do projeto. Para mais informações acessar:

Web Server Configuration Laravel

## Instalação do projeto
É necessário seguir os passos abaixo para a instalação do projeto.

## Clone do projeto
Para efetuar o clone do projeto, vá até o diretório onde ele ficará hospedado e execute o comando abaixo:

$ git clone https://gitlab.com/projeto-de-desenvolvimento/tcc-pedro/integrador.git .

## Instalando as dependências (Composer)
Para instalar as dependências do projeto é necessário rodar o composer, para isso, digite na raíz do projeto:

$ composer install

Aguarde até a finalização da instalação das dependências e prossiga para a próxima etapa.

## Configurando o projeto
É necessário executar alguns comandos para que o framework do projeto funcione da maneira adequada. Para isso, na raíz do projeto execute os seguintes comandos:

$ cp .env.example .env

$ php artisan key:generate

## Configurando: Parte 2
Após a execução dos comandos, é necessário realizar a edição do arquivo .env conforme as instruções abaixo:

APP_URL: o endereço completo do site.

DB_HOST: o ip/host do servidor de banco de dados onde será hospedado o banco do projeto.

DB_PORT: a porta do servidor de banco de dados.

DB_DATABASE: o nome do banco de dados que será criado no servidor. Lembrando que o collation do mesmo deverá ser utf8.

DB_USERNAME: o usuário do banco de dados.

DB_PASSWORD: a senha do banco de dados. Caso a senha tenha caracteres especiais, colocar a mesma entre aspas duplas.

Exemplo de arquivo já configurado:

<<<<<<< HEAD
```
=======
>>>>>>> master
APP_NAME=Integrador
APP_ENV=local
APP_KEY=base64:VT99deNIUw2pP3b92Pw3P+gtkCHDt58BVMeIeFrPWZw=
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=http://localhost

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=integrador
DB_USERNAME=root
DB_PASSWORD=
```

## Importando o banco de dados
Após a configuração do projeto, é necessário realizar a importação do banco de dados do site e popular a tabela de tipos de tabelas de tipos. Para isso, crie um banco com o nome integrador com o collation utf8 e executar os seguintes comandos dentro do diretório raiz do projeto:

$ php artisan migrate

$ php artisan db:seed --class=TipoSeeder

## Vinculando o Storage ao caminho das imagens de atendimentos salvas no banco de dados:
Para realizar esse vínculo, apague a pasta storage dentro da pasta public e execute os seguintes comandos:

$ php artisan storage:link


## Considerações finais
Com tudo configurado, agora é só acessar o endereço que você configurou para o site!

## Publicação
Instruções para teste do Sistema Integrador CTCP 
Credenciais: 
Endereço: https://integradorapi.herokuapp.com/
Login: setrpel@gmail.com
Senha: 180481pd
Datas importadas do API para consultas 01/11/2020 ate 03/11/2020.

Instruções para teste do Sistema Simulado API
Credenciais:
Endereço: https://sensationnel-vin-78942.herokuapp.com/
Login: setrpel@gmail.com
Senha: 180481pd
