<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Linha;
use Illuminate\Support\Facades\DB;
class Atendimento extends Model
{
     //
     protected $table = 'atendimentos';
     protected $primaryKey = 'id';
     protected $fillable = ['id', 'users_id', 'linhas_id', 'nome','descricao', 'foto',
         'km_ida', 'km_volta', 'codatendimento', 'ativo'];
 
     public function users()
     {
         return $this->belongsTo('App\User');
     }
     public function linhas()
     {
         return $this->belongsTo('App\Linha');
     }
     
    public static function ativo() {
        return ['Ativo', 'Inativo'];
    }
    public function getAtivoAttribute($value) {
        if ($value=="1") {
            return "Ativo";
        } else if ($value == "0") {
            return "Inativo";
        } 
    }
    public function setAtivoAttribute($value) {
        if ($value == "Ativo") {
            $this->attributes['ativo'] = "1";
        } else if ($value == "Inativo") {
            $this->attributes['ativo'] = "0";
        } 
    }        

    // retira a máscara com "." e "," antes da inserção 
    public function setKmIdaAttribute($value) {
        $novo1 = str_replace('.', '', $value);    // retira o ponto
        $novo2 = str_replace(',', '.', $novo1);   // substitui a , por .
        $this->attributes['km_ida'] = $novo2;
}

public function setKmVoltaAttribute($value) {
    $novo1 = str_replace('.', '', $value);    // retira o ponto
    $novo2 = str_replace(',', '.', $novo1);   // substitui a , por .
    $this->attributes['km_volta'] = $novo2;


}
}