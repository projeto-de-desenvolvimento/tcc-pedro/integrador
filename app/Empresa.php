<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Funcionario;
class Empresa extends Model
{
    //
    protected $table = 'empresas';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'users_id', 'nome', 'endereco','descricao',
        'cep', 'cnpj', 'telefone', 'email', 'ativo'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public static function ativo() {
        return ['Ativo', 'Inativo'];
    }
    public function getAtivoAttribute($value) {
        if ($value=="1") {
            return "Ativo";
        } else if ($value == "0") {
            return "Inativo";
        } 
    }
    public function setAtivoAttribute($value) {
        if ($value == "Ativo") {
            $this->attributes['ativo'] = "1";
        } else if ($value == "Inativo") {
            $this->attributes['ativo'] = "0";
        } 
    }
   
}