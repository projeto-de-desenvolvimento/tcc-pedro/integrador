<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Empresa;
class Funcionario extends Model
{
     //
     protected $table = 'funcionarios';
     protected $primaryKey = 'id';
     protected $fillable = ['id', 'nome', 'profissao','matricula','empresas_id', 'nascimento','descricao',
         'cpf', 'curso_data', 'endereco', 'telefone', 'email','ativo','users_id'];
 
         public function empresas()
     {
         return $this->belongsTo('App\Empresa');
     }
     public function users()
     {
         return $this->belongsTo('App\User');
     }
     
     
    
    public static function profissao() {
        return ['Cobrador', 'Motorista'];
    }
    public function getProfissaoAttribute($value) {
        if ($value=="1") {
            return "Motorista";
        } else if ($value == "2") {
            return "Cobrador";
        } 
    }
    public function setProfissaoAttribute($value) {
        if ($value == "Motorista") {
            $this->attributes['profissao'] = "1";
        } else if ($value == "Cobrador") {
            $this->attributes['profissao'] = "2";
        } 
    }       
    public static function ativo() {
        return ['Ativo', 'Inativo'];
    }
    public function getAtivoAttribute($value) {
        if ($value=="1") {
            return "Ativo";
        } else if ($value == "0") {
            return "Inativo";
        } 
    }
    public function setAtivoAttribute($value) {
        if ($value == "Ativo") {
            $this->attributes['ativo'] = "1";
        } else if ($value == "Inativo") {
            $this->attributes['ativo'] = "0";
        } 
    }        


}