<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Storage;
use App\Atendimento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Linha;
use App\Tipo;
use App\Horario;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Support\Facades\DB;
class AtendimentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dados = Atendimento::paginate(30);
        // obtém a soma do campo preço
        $numAtendimentos = Atendimento::count('id'); // obtém o número de registros
        return view('admin.atendimentos_list', ['atendimentos' => $dados,
            'numAtendimentos' => $numAtendimentos]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('name')->get();
        $linhas = Linha::orderBy('nome')->get();
        $atendimentos = Atendimento::orderBy('nome')->get();
        return view('admin.atendimentos_form', ['users' => $users,
        'linhas' => $linhas,'atendimentos' => $atendimentos, 'acao' => 1]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'min:4|max:100|unique:atendimentos',
            'descricao'=> 'max:250',
            'km_ida'=> 'max:5',
            'km_volta'=> 'max:5',
            'linhas_id' => 'required' 
       ]); 
        // obtém todos os campos vindos do form
       
        $dados = $request->all();
        // se o usuário informou a foto e a imagem foi corretamente enviada
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {
            $path = $request->file('foto')->store('fotos');
            $dados['foto'] = $path;
        }
        $inc = Atendimento::create($dados);
        if ($inc) {
            return redirect()->route('atendimentos.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $linhas = Linha::orderBy('nome')->get();
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Atendimento::find($id);
        $atendimentos = Atendimento::orderBy('nome')->get();
        return view('admin.atendimentos_form', ['reg' => $reg,'linhas' => $linhas,
                                           'acao' => 2]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'min:4|max:100',
            'descricao'=> 'max:250',
            'ativo'=> 'required',
            'linhas_id'=> 'required' 
       ]); 
        // obtém os dados do form
        $dados = $request->all();
        // posiciona no registo a ser alterado
        $reg = Atendimento::find($id);
        // se o usuário informou a foto e a imagem foi corretamente enviada
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {
            if (Storage::exists($reg->foto)) {
                Storage::delete($reg->foto);
            }
            $path = $request->file('foto')->store('fotos');
            $dados['foto'] = $path;
        }
        // realiza a alteração
        $alt = $reg->update($dados);
        if ($alt) {
            return redirect()->route('atendimentos.index')
                            ->with('status', $request->nome . ' Alterado!');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $atendimento = Atendimento::find($id);
        if ($atendimento->delete()) {
            return redirect()->route('atendimentos.index')
                            ->with('status', $atendimento->nome . ' Excluído!');
        }
    }

    public function atendimentosFiltro()
    {
        $atendimentos = Atendimento::orderBy('nome')->get();
        return view('admin.atendimentos_filtro', ['atendimentos' => $atendimentos, 'acao' => 1,
           ]);

    }

    public function atendimentosRelatorio(Request $request)
    {
        $this->validate($request, [
            'atendimentos_id' => 'required'
       ]); 
        $atendimentos_id = $request->atendimentos_id;

        $sql = "SELECT atendimentos.nome, atendimentos.foto, atendimentos.codatendimento, 
        atendimentos.descricao, linhas.nome AS linha, atendimentos.km_ida, atendimentos.km_volta
        FROM atendimentos
        INNER JOIN linhas ON linhas.id = atendimentos.linhas_id 
        AND atendimentos.id = $atendimentos_id";

        $atendimentos = DB::select($sql);

        $sqlUteisIda = "SELECT COUNT(horarios.codatendimento) AS km FROM horarios 
        INNER JOIN atendimentos ON atendimentos.codatendimento = horarios.codatendimento
        AND horarios.tipodia LIKE 'UTEIS'
        AND horarios.sentido LIKE 'CENTRO'
        AND atendimentos.id = $atendimentos_id";

        $kmui = DB::select($sqlUteisIda);

        $sqlUteisVolta = "SELECT COUNT(horarios.codatendimento) AS km FROM horarios 
        INNER JOIN atendimentos ON atendimentos.codatendimento = horarios.codatendimento
        AND horarios.tipodia LIKE 'UTEIS'
        AND horarios.sentido LIKE 'BAIRRO'
        AND atendimentos.id = $atendimentos_id";

        $kmuv = DB::select($sqlUteisVolta);

        $sqlSabadosIda = "SELECT COUNT(horarios.codatendimento) AS km FROM horarios 
        INNER JOIN atendimentos ON atendimentos.codatendimento = horarios.codatendimento
        AND horarios.tipodia LIKE 'SABADOS'
        AND horarios.sentido LIKE 'CENTRO'
        AND atendimentos.id = $atendimentos_id";

        $kmsi = DB::select($sqlSabadosIda);

        $sqlSabadosVolta = "SELECT COUNT(horarios.codatendimento) AS km FROM horarios 
        INNER JOIN atendimentos ON atendimentos.codatendimento = horarios.codatendimento
        AND horarios.tipodia LIKE 'SABADOS'
        AND horarios.sentido LIKE 'BAIRRO'
        AND atendimentos.id = $atendimentos_id";

        $kmsv = DB::select($sqlSabadosVolta);

        $sqlDomingosIda = "SELECT COUNT(horarios.codatendimento) AS km FROM horarios 
        INNER JOIN atendimentos ON atendimentos.codatendimento = horarios.codatendimento
        AND horarios.tipodia LIKE 'DOMINGOS'
        AND horarios.sentido LIKE 'CENTRO'
        AND atendimentos.id = $atendimentos_id";

        $kmdi = DB::select($sqlDomingosIda);

        $sqlDomingosVolta = "SELECT COUNT(horarios.codatendimento) AS km FROM horarios 
        INNER JOIN atendimentos ON atendimentos.codatendimento = horarios.codatendimento
        AND horarios.tipodia LIKE 'DOMINGOS'
        AND horarios.sentido LIKE 'BAIRRO'
        AND atendimentos.id = $atendimentos_id";

        $kmdv = DB::select($sqlDomingosVolta);

        
        return view('admin.atendimentos_rel', ['atendimentos' => $atendimentos , 'kmui' => $kmui
        , 'kmuv' => $kmuv, 'kmsi' => $kmsi, 'kmsv' => $kmsv, 'kmdi' => $kmdi, 'kmdv' => $kmdv]);

    }
}