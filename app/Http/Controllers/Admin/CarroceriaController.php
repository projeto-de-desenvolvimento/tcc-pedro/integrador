<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Carroceria;
use App\User;
use Illuminate\Http\Request;

class CarroceriaController extends Controller
{
//
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dados = Carroceria::paginate(30);
        $numCarrocerias = Carroceria::count('id');
        return view('admin.carrocerias_list', ['carrocerias' => $dados, 'numCarrocerias'=>$numCarrocerias]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
 

    public function create()

    {
        $users = User::orderBy('name')->get();
        $carrocerias = Carroceria::orderBy('nome')->get();

        return view('admin.carrocerias_form', ['users'=> $users,'acao' => 1]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'min:4|max:100|unique:carrocerias',
            'descricao'=> 'max:250',
            'ativo'=> 'required' 
      
           
       ]);      
       
        // obtém todos os campos vindos do form
        $dados = $request->all();
        $inc = Carroceria::create($dados);
        if ($inc) {
            return redirect()->route('carrocerias.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }else {
 
            return redirect()->route('carrocerias.index')
             ->with('status','Não foi possível cadastrar');  
            }  
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // posiciona no registro a ser alterado e obtém seus dados
        $reg = Carroceria::find($id);
        $linhas = Carroceria::orderBy('nome')->get();
        return view('admin.carrocerias_form', ['reg' => $reg, 'acao' => 2]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'min:4|max:100|',
            'descricao'=> 'max:250',
            'ativo'=> 'required' 
           
       ]);     
           
      
        // obtém os dados do form
        $dados = $request->all();
        // posiciona no registo a ser alterado
        $reg = Carroceria::find($id);
        // se o usuário informou a foto e a imagem foi corretamente enviada
        // realiza a alteração
        $alt = $reg->update($dados);
        if ($alt) {
            return redirect()->route('carrocerias.index')
                ->with('status', $request->nome . ' Alterado!');
        }else {
 
            return redirect()->route('carrocerias.index')
             ->with('status','Não foi possível cadastrar');  
            }  
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $carrocerias = Carroceria::find($id);
        if ($carrocerias->delete()) {
            return redirect()->route('carrocerias.index')
                ->with('status', $carrocerias->nome . ' Excluído!');
        }
    }
}
