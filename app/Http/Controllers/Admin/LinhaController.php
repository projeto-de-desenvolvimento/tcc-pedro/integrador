<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Linha;
use App\Horario;
use App\Tipo;
class LinhaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dados = Linha::paginate(30);
        // obtém a soma do campo preço
        $numLinhas = Linha::count('id'); // obtém o número de registros
        return view('admin.linhas_list', ['linhas' => $dados,
            'numLinhas' => $numLinhas]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $linhas = Linha::orderBy('nome')->get();
        return view('admin.linhas_form', ['linhas' => $linhas,  'acao' => 1]);    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'nome' => 'min:4|max:100|unique:linhas',
            'descricao'=> 'max:250',
            'ativo'=> 'required' 
      
           
       ]);  
        // obtém todos os campos vindos do form
        $dados = $request->all();
        // se o usuário informou a foto e a imagem foi corretamente enviada
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {
            $path = $request->file('foto')->store('fotos');
            $dados['foto'] = $path;
        }
        $inc = Linha::create($dados);
        if ($inc) {
            return redirect()->route('linhas.index')
                ->with('status', $request->nome . ' inserido com sucesso');
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
         // posiciona no registro a ser alterado e obtém seus dados
         $reg = Linha::find($id);
         $linhas = Linha::orderBy('nome')->get();
         return view('admin.linhas_form', ['reg' => $reg,  
                                           'acao' => 2]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nome' => 'min:4|max:100',
            'descricao'=> 'max:250',
            'ativo'=> 'required' 
      
           
       ]);  
        // obtém os dados do form
        $dados = $request->all();
        // posiciona no registo a ser alterado
        $reg = Linha::find($id);
        // se o usuário informou a foto e a imagem foi corretamente enviada
        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {
            if (Storage::exists($reg->foto)) {
                Storage::delete($reg->foto);
            }
            $path = $request->file('foto')->store('fotos');
            $dados['foto'] = $path;
        }
        // realiza a alteração
        $alt = $reg->update($dados);
        if ($alt) {
            return redirect()->route('linhas.index')
                            ->with('status', $request->nome . ' Alterado!');
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $linha = Linha::find($id);
        if ($linha->delete()) {
            return redirect()->route('linhas.index')
                            ->with('status', $linha->nome . ' Excluído!');
        }
    }
}
