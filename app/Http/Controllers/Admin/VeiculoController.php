<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\Auth;
use App\Veiculo;
use App\Funcionario;
use App\Viagem;
use App\Empresa;
use App\Carroceria;
use App\Chassi;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class VeiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $dados = Veiculo::paginate(30);
        $numVeiculos = Veiculo::count('id'); 
        return view('admin.veiculos_list', ['linhas' => $dados,
            'numVeiculos' => $numVeiculos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
        $empresas = Empresa::orderBy('nome')->get();
        $carrocerias = Carroceria::orderBy('nome')->get();
        $chassis = Chassi::orderBy('nome')->get();
        $users = User::orderBy('name')->get();
       
        return view('admin.veiculos_form', ['empresas' => $empresas,'carrocerias' => $carrocerias,
        'chassis' => $chassis,
            'users' => $users, 'acao' => 1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'prefixo' => 'min:1|max:4|unique:veiculos',
            'empresas_id'=> 'required',
            'banco'=> 'max:2',
            'lotacao'=> 'max:3',
            'placa'=> 'max:7',
            'ano_modelo'=> 'max:10',
            'ano_fabricacao'=> 'max:10',
            'descricao'=> 'max:250',
            'ativo'=> 'required' 
      
           
       ]);  

        //

         // obtém todos os campos vindos do form
         $dados = $request->all();
         $inc = Veiculo::create($dados);
         if ($inc) {
             return redirect()->route('veiculos.index')
                 ->with('status', $request->prefixo . ' inserido com sucesso');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Veiculo  $veiculo
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Veiculo  $veiculo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $reg = Veiculo::find($id);

        $empresas = Empresa::orderBy('nome')->get();
        $carrocerias = Carroceria::orderBy('nome')->get();
        $chassis = Chassi::orderBy('nome')->get();
               
        return view('admin.veiculos_form', ['reg' => $reg, 'empresas' => $empresas, 'carrocerias' => $carrocerias,
        'chassis' => $chassis, 'acao' => 2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Veiculo  $veiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'prefixo' => 'min:1|max:4',
            'empresas_id'=> 'required',
            'banco'=> 'max:2',
            'lotacao'=> 'max:3',
            'placa'=> 'max:7',
            'ano_modelo'=> 'max:10',
            'ano_fabricacao'=> 'max:10',
            'descricao'=> 'max:250',
            'ativo'=> 'required' 
       ]);  
         // obtém os dados do form
         $dados = $request->all();
         // posiciona no registo a ser alterado
         $reg = Veiculo::find($id);
         // se o usuário informou a foto e a imagem foi corretamente enviada
         // realiza a alteração
         $alt = $reg->update($dados);
         if ($alt) {
             return redirect()->route('veiculos.index')
                 ->with('status', $request->prefixo . ' Alterado!');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Veiculo  $veiculo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $veiculos = Veiculo::find($id);
        if ($veiculos->delete()) {
            return redirect()->route('veiculos.index')
                            ->with('status', $veiculos->prefixo . ' Excluído!');
        }
    }

    public function veiculosFiltro()
    {
        $veiculos = Veiculo::orderBy('prefixo')->get();
        return view('admin.veiculos_filtro', ['veiculos' => $veiculos, 'acao' => 1,
           ]);

    }

    public function veiculosRelatorio(Request $request)
    {
        $inicio = $request->inicio;
        $fim = $request->fim;
        $veiculos = $request->veiculos;
        $this->validate($request, [
            'inicio' => 'required',
            'fim' => 'required'

       ]); 
$SQL="SELECT DISTINCT c.prefixo, v.data
from veiculos c, viagens v WHERE c.id = $veiculos and v.data >= '$inicio' 
and v.data <= '$fim' and c.prefixo = v.veiculo";


$distveiculos= DB::select($SQL);
$numdias= "SELECT COUNT(DISTINCT v.data) as dias 
from veiculos c, viagens v WHERE c.id = $veiculos and v.data >= '$inicio' 
and v.data <= '$fim' and c.prefixo = v.veiculo";    
 $dias= DB::select($numdias);
 return \PDF::loadView('admin.veiculos_rel', ['distveiculos' => $distveiculos, 'dias'=>$dias, 
 'veiculos'=>$veiculos, 'inicio'=>$inicio, 'fim'=>$fim ])->stream();

    }

    }