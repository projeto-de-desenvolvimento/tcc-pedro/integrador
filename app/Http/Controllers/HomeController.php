<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Linha;
use App\Tipo;
use App\Atendimento;
use App\Horario;

class HomeController extends Controller
{
    public function index()
    {
      $sqlTipo = "SELECT tipos.nome from tipos
         where tipos.id = 0";

        $tipo = DB::select($sqlTipo);

        $sqlLinha = "SELECT linhas.nome from linhas
        where linhas.id = 0";
       $linha = DB::select($sqlLinha);

        $linhas = Linha::orderBy('nome')->where('ativo', 1)->get();

        $tipos = Tipo::orderBy('nome','DESC')->get();
        $sql = "SELECT horario, atendimentos.nome AS atendimento,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
        INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
      where tipos.nome = 'ÚTEIS1' and horarios.sentido = 'CENTRO' order by horarios.horario";
        $centro = DB::select($sql);
        $bairro = DB::select($sql);
        return view('home', ['centro' => $centro, 'bairro' => $bairro,'linhas' => $linhas, 'linha' => $linha,'tipos' => $tipos, 'tipo' => $tipo])->with('msg', ' Excluído!');
    }
    public function horariosHome(Request $request)
    {
      
      $linhas_id = $request->linhas_id;
      $tipos_id = $request->tipos_id;
      $linhas = Linha::orderBy('nome')->where('ativo', 1)->get();
      $tipos = Tipo::orderBy('nome','DESC')->get();
        
        $sqlC = "SELECT horario, atendimentos.nome AS atendimento,
        tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
        INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
        INNER join linhas on atendimentos.linhas_id = linhas.id
         where tipos.id = $tipos_id and horarios.sentido = 'CENTRO' 
         and linhas.id = $linhas_id
         order by horarios.horario";
        $centro = DB::select($sqlC);
         $sqlB = "SELECT horario, atendimentos.nome AS atendimento,
         tipos.nome, horarios.sentido FROM horarios INNER join tipos on horarios.tipodia = tipos.nome
         INNER join atendimentos on horarios.codatendimento = atendimentos.codatendimento
         INNER join linhas on atendimentos.linhas_id = linhas.id
          where tipos.id = $tipos_id and horarios.sentido = 'BAIRRO' 
          and linhas.id = $linhas_id
          order by horarios.horario";
        $bairro = DB::select($sqlB);
        $sqlLinha = "SELECT linhas.nome from linhas
         where linhas.id = $linhas_id";
        $linha = DB::select($sqlLinha);
        $sqlTipo = "SELECT tipos.nome as tipos, linhas.nome as linhas from tipos, linhas
         where tipos.id = $tipos_id
         and linhas.id = $linhas_id";
        $tipo = DB::select($sqlTipo);
        
        return view('home', ['centro' => $centro, 'bairro' => $bairro,
        'linhas' => $linhas, 'linha'=> $linha,
        'tipos' => $tipos, 'tipo' => $tipo]);
        
      }
    
  } 
