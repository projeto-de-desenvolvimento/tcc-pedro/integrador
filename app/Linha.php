<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{

    protected $fillable = ['nome', 'descricao', 'users_id','descricao', 'ativo' ];
    public function users() {
        return $this->belongsTo('App\User');
    }
    public static function ativo() {
        return ['Ativo', 'Inativo'];
    }
    public function getAtivoAttribute($value) {
        if ($value=="1") {
            return "Ativo";
        } else if ($value == "0") {
            return "Inativo";
        } 
    }
    public function setAtivoAttribute($value) {
        if ($value == "Ativo") {
            $this->attributes['ativo'] = "1";
        } else if ($value == "Inativo") {
            $this->attributes['ativo'] = "0";
        } 
    }        
    
}
