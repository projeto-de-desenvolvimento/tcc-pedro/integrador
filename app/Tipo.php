<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use App\User;
class Tipo extends Model
{
    //
    protected $fillable = ['id','nome', 'descricao', 'users_id', 'ativo'];
    

    public function users()
    {
        return $this->belongsTo('App\User');
    }
    public static function ativo()
    {
        return ['Ativo', 'Inativo'];
    }
    public function getAtivoAttribute($value)
    {
        if ($value=="1") {
            return "Ativo";
        } elseif ($value == "0") {
            return "Inativo";
        }
    }
    public function setAtivoAttribute($value)
    {
        if ($value == "Ativo") {
            $this->attributes['ativo'] = "1";
        } elseif ($value == "Inativo") {
            $this->attributes['ativo'] = "0";
        }
    }
}