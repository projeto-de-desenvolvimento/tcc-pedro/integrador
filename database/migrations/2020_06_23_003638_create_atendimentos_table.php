<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAtendimentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atendimentos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('linhas_id');
            $table->string('nome',100)->unique();
            $table->string('foto',100)->nullable($value = true);
            $table->double('km_ida',4,3)->nullable($value = true);
            $table->double('km_volta',4,3)->nullable($value = true);
            $table->double('codatendimento',9,0)->nullable($value = true);
            $table->string('descricao',250)->nullable($value = true);
            $table->tinyInteger('ativo');
            $table->timestamps();
            $table->foreign('users_id')->references('id')->on('users');
            $table->foreign('linhas_id')->references('id')->on('linhas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atendimentos');
    }
}
