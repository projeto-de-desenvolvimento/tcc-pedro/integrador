<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codatendimento');
            $table->string('horario',100);
            $table->string('tipodia');
            $table->string('sentido',100);
            $table->string('data',100);
            $table->unsignedInteger('users_id')->nullable($value = true);
            $table->timestamps();
            $table->foreign('users_id')->references('id')->on('users');
            
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horarios');
    }
}
