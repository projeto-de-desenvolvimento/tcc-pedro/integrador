<?php
use Illuminate\Database\Seeder;
class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         
        DB::table('tipos')->insert([
            'nome' => 'UTEIS',
            'descricao' => 'Dias Úteis',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
            'users_id' => 1,
            'ativo' => 1
        ]);
        DB::table('tipos')->insert([
            'nome' => 'SABADOS',
            'descricao' => 'Tabelas de Sábados',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
            'users_id' => 1,
            'ativo' => 1
        ]);
        DB::table('tipos')->insert([
            'nome' => 'DOMINGOS',
            'descricao' => 'Tabelas de Domingos e Feriados',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s'),
            'users_id' => 1,
            'ativo' => 1
        ]);
    }
}
