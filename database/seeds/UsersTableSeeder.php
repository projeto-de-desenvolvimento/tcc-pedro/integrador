<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.com.br',
            'password' => '$2y$10$Wiqs.YPbdONJefogp19ORODsoGQcLOqfPmjONFuiT5IbmzOIgWnty',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
            
        ]);
    }
}
