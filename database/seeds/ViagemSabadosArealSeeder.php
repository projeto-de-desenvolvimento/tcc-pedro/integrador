<?php

use Illuminate\Database\Seeder;

class ViagemSabadosArealSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //tabela 101M /s
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => '1002',
            'cobrador' => '2002',
            'inicioprogramado' => '05:30',         
            'fimprogramado' => '06:00',         
            'data' => '02/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'SABADOS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => '1002',
            'cobrador' => '2002',
            'inicioprogramado' => '06:00',         
            'fimprogramado' => '06:25',         
            'data' => '02/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'SABADOS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
       
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => '1002',
            'cobrador' => '2002',
            'inicioprogramado' => '07:30',         
            'fimprogramado' => '08:00',
            'data' => '02/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'SABADOS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => '1002',
            'cobrador' => '2002',
            'inicioprogramado' => '08:00',         
            'fimprogramado' => '08:30',
            'data' => '02/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'SABADOS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
      
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1002',
            'motorista' => '1002',
            'cobrador' => '2002',
            'inicioprogramado' => '10:00',         
            'fimprogramado' => '10:30',
            'data' => '02/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'SABADOS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1002',
            'motorista' => '1002',
            'cobrador' => '2002',
            'inicioprogramado' => '10:30', 
            'fimprogramado' => '10:55',        
            'data' => '02/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'SABADOS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
      
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1001',
            'motorista' => '1002',
            'cobrador' => '2002',
            'inicioprogramado' => '12:00',         
            'fimprogramado' => '12:20',
            'data' => '02/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'SABADOS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1001',
            'motorista' => '1002',
            'cobrador' => '2002',
            'inicioprogramado' => '12:20',         
            'fimprogramado' => '12:55',
            'data' => '02/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'SABADOS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
       
      //tabela 101T
      DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '14:00',         
        'fimprogramado' => '14:30',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '14:30',         
        'fimprogramado' => '14:55',
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
   
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '16:30',         
        'fimprogramado' => '17:00',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '17:00',         
        'fimprogramado' => '17:55',
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1002',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '19:00',         
        'fimprogramado' => '19:30',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1002',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '19:30',         
        'fimprogramado' => '20:55',
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '20:00',         
        'fimprogramado' => '20:30',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '20:30',         
        'fimprogramado' => '20:55',
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
  
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '22:00',         
        'fimprogramado' => '22:30',
        'data' => '02/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => '1001',
        'cobrador' => '2001',
        'inicioprogramado' => '22:30',         
        'fimprogramado' => '23:00',
        'data' => '02/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'SABADOS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    }
}
