@extends('adminlte::page')
@section('title', 'Filtro de Atendimentos')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <h2>Relatório de Atendimentos
       
        <a href="{{ route('atendimentos.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <form method="POST" action="{{ route('atendimentosRelatorio') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
    <div class="container-fluid">
       
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="atendimentos_id">Atendimentos:</label>
                        <select id="atendimentos_id" name="atendimentos_id" class="form-control">
                            @foreach($atendimentos as $a)
                            <option value="{{$a->id}}" {{ ((isset($reg) and 
                            $reg->atendimentos_id == $a->id) or 
                         old('atendimentos_id') == $a->id) ? "selected" : "" }}>
                                {{$a->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                   
                   
                
                <input type="submit" value="Pesquisar" class="btn btn-success">        
               
                </div>

    
        
  
   
    </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
    
   
    });
    </script>
    @endsection
   