@extends('adminlte::page')
@extends('formulario')
@section('title', 'Cadastro de Atendimentos')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ($acao==1)
<h2>Inclusão de Atendimentos
    @elseif ($acao ==2)
    <h2>Alteração de Atendimentos
        @endif
        <a href="{{ route('atendimentos.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <div class="container-fluid">
        @if ($acao==1)
        <form method="POST" action="{{ route('atendimentos.store') }}" enctype="multipart/form-data">
        <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="nome">Nome do Atendimento:</label>
                        <input type="text" id="nome" name="nome" required onkeyup="maiuscula(this)"
                             class="form-control">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="linhas_id">Linha:</label>
                        <select id="linhas_id" name="linhas_id" class="form-control">
                            @foreach($linhas as $l)
                            <option value="{{$l->id}}">
                                {{$l->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <label for="km_ida">Km de Ida:</label>
                        <input type="text" id="km_ida" name="km_ida" required
                             class="form-control">
                    </div>
                </div>

                <div class="col-sm-1">
                    <div class="form-group">
                        <label for="km_volta">Km Volta:</label>
                        <input type="text" id="km_volta" name="km_volta" required
                            class="form-control">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="codatendimento">Cód. Atendimento:</label>
                        <input type="number" id="codatendimento" name="codatendimento" 
                            class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="descricao">Descrição:</label>
                        <textarea input type="text" id="descricao" name="descricao" 
                             class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="foto">Foto:</label>
                        <input type="file" id="foto" name="foto"  class="form-control">
                    </div>
                </div>
                <div class="col-sm-2">
                    <label for="ativo">Status:</label><br>
                    <select name="ativo" class="form-control">
                        <option selected="">
                        </option>
                        <option value="Ativo">Ativo</option>
                        <option value="Inativo">Inativo</option>
                    </select>
                </div>
            </div>
            @elseif ($acao==2)
            <form method="POST" action="{{route('atendimentos.update', $reg->id)}}" enctype="multipart/form-data">
                {!! method_field('put') !!}
                <div class="row">
                
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="nome">Nome do Atendimento:</label>
                        <input type="text" id="nome" name="nome" required onkeyup="maiuscula(this)"
                            value="{{$reg->nome}}" class="form-control">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="linhas_id">Linha:</label>
                        <select id="linhas_id" name="linhas_id" class="form-control">
                            @foreach($linhas as $l)
                            <option value="{{$l->id}}" {{ ((isset($reg) and
                             $reg->linhas_id == $l->id) == $l->id) ? "selected" : "" }}>
                                {{$l->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-1">
                    <div class="form-group">
                        <label for="km_ida">Km de Ida:</label>
                        <input type="text" id="km_ida" name="km_ida" required
                            value="{{number_format($reg->km_ida, 1, ',', '.')}}" class="form-control">
                    </div>
                </div>

                <div class="col-sm-1">
                    <div class="form-group">
                        <label for="km_volta">Km Volta:</label>
                        <input type="text" id="km_volta" name="km_volta" required
                            value="{{number_format($reg->km_volta, 1, ',', '.')}}" class="form-control">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="codatendimento">Cód. Atendimento:</label>
                        <input type="number" id="codatendimento" name="codatendimento" 
                            value="{{$reg->codatendimento}}" class="form-control">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="descricao">Descrição:</label>
                        <textarea input type="text" id="descricao" name="descricao" 
                            value="{{$reg->descricao}}" class="form-control">{{$reg->descricao}}</textarea>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="foto">Foto:</label>
                        <input type="file" id="foto" name="foto"  class="form-control">
                    </div>
                </div>
                <div class="col-sm-2">
                    <label for="ativo">Status:</label><br>
                    <select name="ativo" class="form-control">
                        <option selected="{{$reg->ativo}}">{{$reg->ativo }}
                        </option>
                        <option value="Ativo">Ativo</option>
                        <option value="Inativo">Inativo</option>
                    </select>
                </div>
            </div>
                @endif
                {{ csrf_field() }}
               
                
                <input type="hidden" id="users_id" name="users_id" required value="{{ Auth::user()->id }}">
                <input type="submit" value="Enviar" class="btn btn-success">
                
            </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#km_ida').mask('#.###.##0,0', {
            reverse: true
        });
        $('#km_volta').mask('#.###.##0,0', {
            reverse: true
        });
    });
    </script>
   

    @endsection
    @section('content')
    