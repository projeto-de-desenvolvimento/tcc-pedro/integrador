@extends('adminlte::page')
@section('title', 'Cadastro de Atendimentos')
@section('content_header')
<h1>Cadastro de Atendimentos
    <a href="{{ route('atendimentos.create') }}" class="btn btn-primary pull-right" role="button">Novo</a>
</h1>
@stop
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<table class="table table-striped">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Linha</th>
            <th>Km de Ida</th>
            <th>Km de Volta</th>
            <th>Cod. Atendimento</th>
            <th>Alteração</th>
            <th>Foto</th>
            <th>Obs.</th>
            <th>Status</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($atendimentos as $l)
        <tr>
            <td> {{$l->nome}} </td>
            <td> {{$l->linhas->nome}} </td>
            <td> {{number_format($l->km_ida, 1, ',', '.')}}</td>
            <td> {{number_format($l->km_volta, 1, ',', '.')}} </td>
            <td> {{$l->codatendimento}} </td>
            <td> {{$l->users->name}} </td>
            <td>
                @if (Storage::exists($l->foto))
                <img src="{{url('storage/'.$l->foto)}}" style="width: 100px; height: 60px" alt="Foto">
                @else
                <img src="{{url('storage/fotos/semfoto.png')}}" style="width: 100px; height: 60px" alt="Foto">
                @endif
            </td>
            
            <td> {{$l->descricao}} </td>
            <td> {{$l->ativo}} </td>
            <td>
                <a href="{{route('atendimentos.edit', $l->id)}}" class="btn btn-warning btn-sm" title="Alterar"
                    role="button"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;
            <!--    <form style="display: inline-block" method="post" action="{{route('atendimentos.destroy', $l->id)}}"
                    onsubmit="return confirm('Confirma Exclusão?')">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <button type="submit" title="Excluir" class="btn btn-danger btn-sm"><i
                            class="far fa-trash-alt"></i></button>
                </form>
                !-->
            </td>

            @if ($loop->iteration == $loop->count)
        <tr>
            <td colspan=8>Total de Atendimentos cadastrados: {{$numAtendimentos}}
            </td>
        </tr>
        @endif
        @empty
        <tr>
            <td colspan=8> Não há Atendimentos cadastradas ou
                para o filtro informado </td>
        </tr>
        @endforelse
    </tbody>
</table>
{{ $atendimentos->links() }}
@stop
@section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection