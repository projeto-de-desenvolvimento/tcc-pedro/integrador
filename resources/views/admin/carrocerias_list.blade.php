@extends('adminlte::page')
@section('title', 'Cadastro de Carrocerias')
@section('content_header')
    <h1>Cadastro de Carrocerias
    <a href="{{ route('carrocerias.create') }}" class="btn btn-primary pull-right" role="button">Novo</a>
    </h1>
@stop
@section('content')
@if (session('status'))
   <div class="alert alert-success">
      {{ session('status') }}
   </div> 
@endif
<table class="table table-striped">
  <thead>
    <tr>
      <th>Nome</th>
      <th>Endereço</th>
      <th>Telefone</th>
      <th>E-mail</th>
      <th>Descrição</th>
      <th>Alteração</th>
      <th>Status</th>
      <th>Ações</th>
      
      
    </tr>
  </thead>
  <tbody>
    @forelse ($carrocerias as $c)
      <tr>
        <td> {{$c->nome}} </td>
        <td> {{$c->endereco}} </td>
        <td> {{$c->telefone}} </td>
        <td> {{$c->email}} </td>
        <td> {{$c->descricao}} </td>
        <td> {{$c->users->name}} </td>
        <td> {{$c->ativo}} </td>
        <td>
                <a href="{{route('carrocerias.edit', $c->id)}}" class="btn btn-warning btn-sm" title="Alterar"
                    role="button"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;
               <!-- <form style="display: inline-block" method="post" action="{{route('carrocerias.destroy', $c->id)}}"
                    onsubmit="return confirm('Confirma Exclusão?')">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <button type="submit" title="Excluir" class="btn btn-danger btn-sm"><i
                            class="far fa-trash-alt"></i></button>
                </form> !-->
            </td>
            @if ($loop->iteration == $loop->count)
        <tr>
            <td colspan=8>Total de carrocerias cadastradas: {{$numCarrocerias}}
            </td>
        </tr>
        @endif
        @empty
        <tr>
            <td colspan=8> Não há carrocerias cadastradas ou
                para o filtro informado </td>
        </tr>
        @endforelse
    </tbody>
    
</table>

{{ $carrocerias->links() }}
@stop
@section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection