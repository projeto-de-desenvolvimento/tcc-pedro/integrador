@extends('adminlte::page')
@section('title', 'Filtro de Empresas')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <h2>Relatório da Quilometragem Percorrida pelas Empresas
       
        <a href="{{ route('empresas.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <form method="POST" action="{{ route('empresasRelatorio') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
    <div class="container-fluid">
       
                
    <div class="col-sm-3">
                   
                        <label for="inicio">Data Início:</label>
                            <input type="text" id="inicio" name="inicio" 
                             class="form-control">
                             <label for="fim">Data Final:</label>
                            <input type="text" id="fim" name="fim" 
                             class="form-control">
                             
   
                             </div>                
                   
                
                <input type="submit" value="Pesquisar" class="btn btn-success">        
               
                </div>
    
        
  
   
    </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
   
    $(document).ready(function() {
        $('#inicio').mask("99/99/9999");
        $('#fim').mask("99/99/9999");
        
    });
    </script>
   
    @endsection
   