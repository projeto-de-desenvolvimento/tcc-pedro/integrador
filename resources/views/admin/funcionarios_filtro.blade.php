@extends('adminlte::page')
@section('title', 'Filtro de Funcionários')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <h2>Relatório de Dias Trabalhados por Funcionário
       
        <a href="{{ route('funcionarios.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <form method="POST" action="{{ route('funcionariosRelatorio') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
    <div class="container-fluid">
       
    
    <div class="col-sm-3">
                    <div class="form-group">
                        <label for="funcionarios">Funcionários:</label>
                        <select id="funcianarios" name="funcionarios" class="form-control">
                            @foreach($funcionarios as $f)
                            <option value="{{$f->id}}" {{ ((isset($reg) and 
                            $reg->funcionarios == $f->id) or 
                         old('funcionarios') == $f->id) ? "selected" : "" }}>
                                {{$f->nome}}</option>
                            @endforeach
                        </select>
                        <label for="inicio">Data Início:</label>
                            <input type="text" id="inicio" name="inicio" 
                             class="form-control">
                             <label for="fim">Data Final:</label>
                            <input type="text" id="fim" name="fim" 
                             class="form-control">
                             
                   
                   
                
                <input type="submit" value="Pesquisar" class="btn btn-success">        
               
                </div>
    
                </div>
  
   
    </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
   $(document).ready(function() {
        $('#inicio').mask("99/99/9999");
        $('#fim').mask("99/99/9999");
        
    });
    </script>
    @endsection
   