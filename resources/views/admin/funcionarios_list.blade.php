@extends('adminlte::page')
@section('title', 'Cadastro de Funcionários')
@section('content_header')
<h1>Cadastro de Funcionários
    <a href="{{ route('funcionarios.create') }}" class="btn btn-primary pull-right" role="button">Novo</a>
</h1>
@stop
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<table class="table table-striped">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Matricula</th>
            <th>Profissão</th>
            <th>Empresa</th>
            <th>Nasc.</th>
            <th>CPF</th>
            <th>Curso</th>
            <th>Endereço</th>
            <th>Telefone</th>
            <th>E-Mail</th>
            <th>Obs.</th>
            <th>Ativo</th>
            <th>Alteração</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($linhas as $l)
        <tr>
            <td> {{$l->nome}} </td>
            <td> {{$l->matricula}} </td>
            <td> {{$l->profissao}}</td>
            <td> {{$l->empresas->nome}} </td>
            <td> {{$l->nascimento}} </td>
            <td> {{$l->cpf}} </td>
            <td> {{$l->curso_data}} </td>
            <td> {{$l->endereco}} </td>
            <td> {{$l->telefone}} </td>
            <td> {{$l->email}} </td>
            <td> {{$l->descricao}} </td>
            <td> {{$l->users->name}} </td>
            <td> {{$l->ativo}} </td>

            <td>
                <a href="{{route('funcionarios.edit', $l->id)}}" class="btn btn-warning btn-sm" title="Alterar"
                    role="button"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;
               <!-- <form style="display: inline-block" method="post" action="{{route('funcionarios.destroy', $l->id)}}"
                    onsubmit="return confirm('Confirma Exclusão?')">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <button type="submit" title="Excluir" class="btn btn-danger btn-sm"><i
                            class="far fa-trash-alt"></i></button>
                </form> !-->
            </td>

            @if ($loop->iteration == $loop->count)
        <tr>
            <td colspan=8>Total de Funcionários cadastrados: {{$numFuncionarios}}
            </td>
        </tr>
        @endif
        @empty
        <tr>
            <td colspan=8> Não há Funcionários cadastrados ou
                para o filtro informado </td>
        </tr>
        @endforelse
    </tbody>
</table>
{{$linhas->links() }}
@stop
@section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection