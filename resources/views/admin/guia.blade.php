@extends('adminlte::page')
@section('title', 'Cadastro de Atendimentos')

@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif
<?php
$pdf = 'storage/fotos/guia.pdf';

header('Content-type: application/pdf');
readfile($pdf);
exit(0);
?>
@stop
@section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection