@extends('adminlte::page')
@section('title', 'Filtro de Guias')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <h2>Geração de Guias de Viagens
       
        <a href="{{ route('horarios.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <form method="POST" action="{{ route('guiaPDF') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
    <div class="container-fluid">
       
                
    
    <div class="col-sm-2">
                        
                            <label for="data">Data:</label>
                            <input type="text" id="data" name="data" 
                             class="form-control">
                                
                           
                            
                        </select>

                        <label for="tabela">Código da Tabela:</label>
                        <select id="tabela" name="tabela" class="form-control">
                            @foreach($viagens as $v)
                            <option value="{{$v->tabela}}" {{ ((isset($reg) and 
                            $reg->tabela == $v->tabela)== $v->tabela) ? "selected" : "" }}>
                                {{$v->tabela}}</option>
                            @endforeach
                        </select>
                   
                
                <input type="submit" value="Pesquisar" class="btn btn-success">        
               
                </div>

    
        
  
   
    </form>
    </div>
    @endsection
    @section('js')
    <script>
    $(document).ready(function() {
        $('#data').mask("99/99/9999");
       
        
    });
  </script>  
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
   
    });
    </script>
    @endsection
   