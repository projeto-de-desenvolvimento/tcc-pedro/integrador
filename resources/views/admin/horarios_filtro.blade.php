@extends('adminlte::page')
@section('title', 'Filtro de Horários')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <h2>Relatório de Horários
       
        <a href="{{ route('horarios.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <form method="POST" action="{{ route('horariosRelatorio') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
    <div class="container-fluid">
       
                
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="linhas_id">Linha:</label>
                        <select id="linhas_id" name="linhas_id" class="form-control">
                            @foreach($linhas as $l)
                            <option value="{{$l->id}}" {{ ((isset($reg) and 
                            $reg->linhas_id == $l->id) or 
                         old('linhas_id') == $l->id) ? "selected" : "" }}>
                                {{$l->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                   
                   
                
                <input type="submit" value="Pesquisar" class="btn btn-success">        
               
                </div>

    
        
  
   
    </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
   
    });
    </script>
    @endsection
   