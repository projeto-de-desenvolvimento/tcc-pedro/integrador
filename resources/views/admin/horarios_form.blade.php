@extends('adminlte::page')
@section('title', 'Cadastro de Horários')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ($acao==1)
<h2>Inclusão de Horários
    @elseif ($acao ==2)
    <h2>Alteração de Horários
        @endif
        <a href="{{ route('horarios.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <div class="container-fluid">
        @if ($acao==1)
        <form method="POST" action="{{ route('horarios.store') }}" enctype="multipart/form-data">
        <div class="col-sm-2">
                    <div class="form-group">
                        <label for="codatendimento">Cod. Atendimento:</label>
                        <select id="codatendimento" name="codatendimento" class="form-control">
                            @foreach($atendimentos as $a)
                            <option value="{{$a->codatendimento}}">
                                {{$a->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tipodia">Tipo de Tabela de Dias:</label>
                        <select id="tipodia" name="tipodia" class="form-control">
                            @foreach($tipos as $t)
                            <option value="{{$t->nome}}" >
                                {{$t->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="horario">Horário:</label>
                        <input type="text" id="horario" name="horario" required
                             class="form-control">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="sentido">Sentido:</label>
                        <select id="sentido" name="sentido" class="form-control">
                                           <option value="" >
                                </option>
                               <option value="Centro">CENTRO</option>
                        <option value="Bairro">BAIRRO</option>
                        </select>
                    </div>
                </div>
            @elseif ($acao==2)
            <form method="POST" action="{{route('horarios.update', $reg->id)}}" enctype="multipart/form-data">
                {!! method_field('put') !!}
                <div class="col-sm-2">
                    <div class="form-group">
                        <label for="codatendimento">Cod. Atendimento:</label>
                        <select id="codatendimento" name="codatendimento" class="form-control">
                            @foreach($atendimentos as $a)
                            <option value="{{$a->codatendimento}}" {{ ((isset($reg) and
                                 $reg->codatendimento == $a->codatendimento) == $a->id) ? "selected" : "" }}>
                                {{$a->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="tipodia">Tipo de Tabela de Dias:</label>
                        <select id="tipodia" name="tipodia" class="form-control">
                        <option value="{{$reg->tipodia}}" >{{$reg->tipodia}}
                                </option>
                        <option value="UTEIS">ÚTEIS</option>
                        <option value="SABADOS">SÁBADOS</option>
                        <option value="DOMINGOS">DOMINGOS</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="horario">Horário:</label>
                        <input type="text" id="horario" name="horario" required
                        value="{{$reg->horario}}" class="form-control">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="sentido">Sentido:</label>
                        <select id="sentido" name="sentido" class="form-control">
                                           <option value="{{$reg->sentido}}" >{{$reg->sentido}}
                                </option>
                               <option value="CENTRO">CENTRO</option>
                        <option value="BAIRRO">BAIRRO</option>
                        </select>
                    </div>
                </div>
                @endif
                {{ csrf_field() }}
               
              
                
                

    <div class="col-sm-4">
    <input type="hidden" id="users_id" name="users_id" required value="{{ Auth::user()->id }}">
    <input type="hidden" id="data" name="data" value="2020-10-18">
        <input type="submit" value="Enviar" class="btn btn-success">
        <input type="reset" value="Limpar" class="btn btn-warning">
    </div>
    </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
    $(document).ready(function() {
        $('#horario').mask('##:##:##', {
            reverse: true
        });
    });
    </script>
    @endsection
   