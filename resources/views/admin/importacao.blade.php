

@extends('adminlte::page')
@section('title', 'Importação')

@section('content_header')


@stop

@section('content')
<div class="alert">
<h4>{{$status}}</h4>
</div>
@stop
@section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection