@extends('adminlte::page')
@section('title', 'Cadastro de Linhas')
@extends('formulario')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if ($acao==1)
<h2>Inclusão de Linhas
    @elseif ($acao ==2)
    <h2>Alteração de Linhas
        @endif
        <a href="{{ route('linhas.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <div class="container-fluid">
        @if ($acao==1)
        <form method="POST" action="{{ route('linhas.store') }}" enctype="multipart/form-data">
        <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nome">Nome da Linha:</label>
                            <input type="text" id="nome" name="nome" required onkeyup="maiuscula(this)"
                                class="form-control">
                        </div>
                    </div>
                
    </div>
    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="descricao">Descrição:</label>
                            <textarea input type="text" id="descricao" name="descricao" 
                                class="form-control"></textarea>
                        </div>
                    </div>
    
    <div class="col-sm-2">
                        <label for="ativo">Status:</label><br>
                        <select name="ativo" class="form-control">
                            <option selected="">
                            </option>
                            <option value="Ativo">Ativo</option>
                            <option value="Inativo">Inativo</option>
                        </select>
                    </div>
                </div>
            @elseif ($acao==2)
            <form method="POST" action="{{route('linhas.update', $reg->id)}}" enctype="multipart/form-data">
                {!! method_field('put') !!}
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="nome">Nome da Linha:</label>
                            <input type="text" id="nome" name="nome" required onkeyup="maiuscula(this)"
                            value="{{$reg->nome}}"
                                class="form-control">
                        </div>
                    </div>
                
    </div>
    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="descricao">Descrição:</label>
                            <textarea input type="text" id="descricao" name="descricao" 
                                value="{{$reg->descricao}}" class="form-control">{{$reg->descricao}}</textarea>
                        </div>
                    </div>
    
    <div class="col-sm-2">
                        <label for="ativo">Status:</label><br>
                        <select name="ativo" class="form-control">
                            <option selected="{{$reg->ativo}}">{{$reg->ativo}}
                            </option>
                            <option value="Ativo">Ativo</option>
                            <option value="Inativo">Inativo</option>
                        </select>
                    </div>
                </div>
                @endif
                {{ csrf_field() }}
               
                <input type="hidden" id="users_id" name="users_id" required value="{{ Auth::user()->id }}">
                <input type="submit" value="Enviar" class="btn btn-success">
                <input type="reset" value="Limpar" class="btn btn-warning">
            </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    @endsection
   