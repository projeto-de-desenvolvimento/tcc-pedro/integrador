@extends('adminlte::page')
@section('title', 'Cadastro de Tipos')
@section('content_header')
    <h1>Cadastro de Tipos
    
@stop
@section('content')
@if (session('status'))
   <div class="alert alert-success">
      {{ session('status') }}
   </div> 
@endif
<table class="table table-striped">
  <thead>
    <tr>
      <th>Nome</th>
      <th>Descrição</th>
      <th>Alteração</th>
      <th>Ativo</th>
      
      
    </tr>
  </thead>
  <tbody>
    @forelse ($tipos as $t)
      <tr>
        <td> {{$t->nome}} </td>
        <td> {{$t->descricao}} </td>
        <td> {{$t->users->name}} </td>
        <td> {{$t->ativo}} </td>
       
            @if ($loop->iteration == $loop->count)
        <tr>
            <td colspan=8>Total de Tipos de Tabelas cadastradas: {{$numTipos}}
            </td>
        </tr>
        @endif
        @empty
        <tr>
            <td colspan=8> Não há Tipos de Tabelas cadastradas ou
                para o filtro informado </td>
        </tr>
        @endforelse
    </tbody>
    
</table>

{{ $tipos->links() }}
@stop
@section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection