@extends('adminlte::page')
@section('title', 'Filtro de Veiculos')
@section('content_header')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

    <h2>Relatório de Veículos Utilizados por Dia
       
        <a href="{{ route('veiculos.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>
    @endsection
    @section('content')
    <form method="POST" action="{{ route('veiculosRelatorio') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
    <div class="container-fluid">
       
                
    <div class="col-sm-2">
                    <div class="form-group">
                        <label for="veiculos">Veículos:</label>
                        <select id="veiculos" name="veiculos" class="form-control">
                            @foreach($veiculos as $v)
                            <option value="{{$v->id}}" {{ ((isset($reg) and 
                            $reg->veiculos == $v->id) or 
                         old('veiculos') == $v->id) ? "selected" : "" }}>
                                {{$v->prefixo}}</option>
                            @endforeach
                        </select>
                        <label for="inicio">Data Início:</label>
                            <input type="text" id="inicio" name="inicio" 
                             class="form-control">
                             <label for="fim">Data Final:</label>
                            <input type="text" id="fim" name="fim" 
                             class="form-control">
                             
                   
                   
                
                <input type="submit" value="Pesquisar" class="btn btn-success">        
               
                </div>
    
                </div>
  
   
    </form>
    </div>
    @endsection
    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>
    <script>
   $(document).ready(function() {
        $('#inicio').mask("99/99/9999");
        $('#fim').mask("99/99/9999");
        
    });
    </script>
    @endsection
   