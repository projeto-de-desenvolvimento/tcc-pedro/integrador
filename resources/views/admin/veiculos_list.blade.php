@extends('adminlte::page')
@section('title', 'Cadastro de Veículos')
@section('content_header')
<h1>Cadastro de Veículos
    <a href="{{ route('veiculos.create') }}" class="btn btn-primary pull-right" role="button">Novo</a>
</h1>

@stop
@section('content')
@if (session('status'))
<div class="alert alert-success">
    {{ session('status') }}
</div>
@endif


<table class="table table-striped">

    <thead>
        <tr>

            <th>Prefixo</th>
            <th>Empresa</th>
            <th>Assentos</th>
            <th>Lotação</th>
            <th>Placa</th>
            <th>Licenciamento</th>
            <th>Ano Modelo</th>
            <th>Ano Fabricação</th>
            <th>Data Vistoria</th>
            <th>Chassi</th>
            <th>Carroceria</th>
            <th>Obs.</th>
            <th>Acess.</th>
            <th>Ativo</th>
            <th>Alteração</th>
            <th>Ações</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($linhas as $l)
        <tr>

            <td> {{$l->prefixo }} </td>
            <td> {{$l->empresas->nome}} </td>
            <td> {{$l->banco}} </td>
            <td> {{$l->lotacao }} </td>
            <td> {{$l->placa }} </td>
            <td> {{$l->licenca_data }} </td>
            <td> {{$l->ano_modelo }} </td>
            <td> {{$l->ano_fabricacao }} </td>
            <td> {{$l->vistoria_data }} </td>
            <td> {{$l->chassis->nome}} </td>
            <td> {{$l->carrocerias->nome}} </td>
            <td> {{$l->descricao}} </td>
            <td> {{$l->acessibilidade}} </td>
            <td> {{$l->ativo}} </td>
            <td> {{$l->users->name}} </td>
            <td>
                <a href="{{route('veiculos.edit', $l->id)}}" class="btn btn-warning btn-sm" title="Alterar"
                    role="button"><i class="fa fa-edit"></i></a>
               <!-- <form style="display: inline-block" method="post" action="{{route('veiculos.destroy', $l->id)}}"
                    onsubmit="return confirm('Confirma Exclusão?')">
                    {{method_field('delete')}}
                    {{csrf_field()}}
                    <button type="submit" title="Excluir" class="btn btn-danger btn-sm"><i
                            class="far fa-trash-alt"></i></button>
                </form> -->
            </td>
            @if ($loop->iteration == $loop->count)
        <tr>
            <td colspan=8>Total de Veículos cadastrados: {{$numVeiculos}}
            </td>
        </tr>
        @endif
        @empty
        <tr>
            <td colspan=8> Não há Veículos cadastrados ou
                para o filtro informado </td>
        </tr>
        @endforelse
    </tbody>

</table>

{{ $linhas->links() }}
@stop
@section('js')
<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"
    integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous">
</script>
@endsection