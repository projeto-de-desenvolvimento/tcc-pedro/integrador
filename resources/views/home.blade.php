@extends('site')
@section('conteudo')

<div div style='text-align:center'>
  <font color=#dfe4ed size="5" >

     <H1> Transportes Avenida</h1>


    <form method="POST" action="{{ route('horariosHome') }}" class="navbar-form navbar-center">
		        	{{ csrf_field() }}
    <div class="container-fluid">
                    <div class="form-group">
                        <label for="linhas_id">Nome da Linha</label>
                        <select id="linhas_id" name="linhas_id" class="form-control">
                            @foreach($linhas as $l)
                            <option value="{{$l->id}}" {{ ((isset($reg) and
                            $reg->linhas_id == $l->id) or
                         old('linhas_id') == $l->id) ? "selected" : "" }}>
                                {{$l->nome}}</option>
                            @endforeach
                        </select>
                    </div>
                        <label for="tipos_id">Tipo de Dia</label>
                        <select id="tipos_id" name="tipos_id" class="form-control">
                            @foreach($tipos as $t)
                            <option value="{{$t->id}}" {{ ((isset($reg) and
                            $reg->tipos_id == $t->id) or
                         old('tipos_id') == $t->id) ? "selected" : "" }}>
                                {{$t->nome}}</option>
                            @endforeach
                        </select>
                        <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span> </button>
                </div>
    </form>
  </font>


  <font color=#dfe4ed >
     <H3 ">
     @forelse($tipo as $t)
     Linha:
        {{ $t->linhas }}
     /
        {{ $t->tipos }}
        @empty
        @endforelse
     </H3>
     </font>
			<div class="thumbnail">
			<div class="modal-body">
				<div class="row">
				<div class="col-md-6">

        <table class="table table-hover" div style='text-align:center'>
        <H3 div style='text-align:center'>CENTRO</H3>
    <thead >
    <tr>
      <th div style='text-align:center'>Linha</th>
      <th div style='text-align:center'>Horário</th>
      </tr>
      </thead>
  <tbody >
    @forelse ($centro as $c)
    <tr>
            <td> {{$c->atendimento}} </td>
            <td> {{$c->horario}} </td>
            </tr>
  @empty
  @endforelse
  </tbody>
</table>
							</div>
				<div class="col-md-6">
        <table class="table table-hover" div style='text-align:center'>
        <H3 div style='text-align:center'>BAIRRO</H3>
    <thead>
    <tr>
    <th div style='text-align:center'>Linha</th>
      <th div style='text-align:center'>Horário</th>
      </tr>
      </thead>
  <tbody>
    @forelse ($bairro as $b)
    <tr>
    <td> {{$b->atendimento}} </td>
            <td> {{$b->horario}} </td>
            </tr>
  @empty
  @endforelse
  </tbody>
</table>
			</div>
					
								<div class="modal-footer">
				</div>
							</form>
					</div>
				</div>
                </div>
	</div>
  </div>
 	<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-latest.min.js"></script>
	<script src="/js/jquery.mask.min.js"></script>
	<script>
  		});
	</script>
  
@endsection
