<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
//site horarios
Route::get('/home', 'HomeController@index')->name('home');
Route::get("/", "HomeController@index")->name('home.index');
Route::post('horariosHome','HomeController@horariosHome')->name('horariosHome');
Route::get('/admin', function () {
    return view('admin.index');
})->middleware('auth');


Route::prefix('admin')->group(function () {
    Route::resource('empresas', 'Admin\EmpresaController')->middleware('auth');
    Route::resource('horarios', 'Admin\HorarioController')->middleware('auth');
    Route::resource('atendimentos', 'Admin\AtendimentoController')->middleware('auth');
    Route::resource('veiculos', 'Admin\VeiculoController')->middleware('auth');
    Route::resource('funcionarios', 'Admin\FuncionarioController')->middleware('auth');
    Route::resource('tipos', 'Admin\TipoController')->middleware('auth');
    Route::resource('chassis', 'Admin\ChassiController')->middleware('auth');
    Route::resource('carrocerias', 'Admin\CarroceriaController')->middleware('auth');
    Route::resource('linhas', 'Admin\LinhaController')->middleware('auth');
    Route::get('/grafuteis', 'Admin\HorarioController@graficoUteis')->middleware('auth');
    Route::get('/grafsabados', 'Admin\HorarioController@graficoSabados')->middleware('auth');
    Route::get('/grafdomingos', 'Admin\HorarioController@graficoDomingos')->middleware('auth');
    Route::get('/horariosPDF', 'Admin\HorarioController@relatorio');
    Route::get('/horariosFiltro', 'Admin\HorarioController@horariosFiltro')->name('horariosFitro')->middleware('auth');
    Route::get('/atendimentosFiltro', 'Admin\AtendimentoController@atendimentosFiltro')->name('atendimentosFitro')->middleware('auth');
    Route::get('/funcionariosFiltro', 'Admin\FuncionarioController@funcionariosFiltro')->name('funcionariosFitro')->middleware('auth');
    Route::get('/empresasFiltro', 'Admin\EmpresaController@empresasFiltro')->name('empresasFitro')->middleware('auth');
    Route::get('/veiculosFiltro', 'Admin\VeiculoController@veiculosFiltro')->name('veiculosFitro')->middleware('auth');
    Route::get('/horariosImportFiltro', 'Admin\HorarioController@horariosImportFiltro')->name('horariosImportFiltro')->middleware('auth');
    Route::post('/horariosImport', 'Admin\HorarioController@horariosImport')->name('horariosImport')->middleware('auth');
    Route::get('/guiasFiltro', 'Admin\HorarioController@guiasFiltro')->name('guiasFitro')->middleware('auth');
    Route::get('/guia', 'Admin\HorarioController@guia')->middleware('auth');;
    Route::get('/importacao', 'Admin\HorarioController@importacao')->middleware('auth');
    Route::post('horariosRelatorio','Admin\HorarioController@horariosRelatorio')->name('horariosRelatorio');
    Route::post('funcioanariosRelatorio','Admin\FuncionarioController@funcionariosRelatorio')->name('funcionariosRelatorio');
    Route::post('empresasRelatorio','Admin\EmpresaController@empresasRelatorio')->name('empresasRelatorio');
    Route::post('veiculosRelatorio','Admin\VeiculoController@veiculosRelatorio')->name('veiculosRelatorio');
    Route::post('atendimentosRelatorio','Admin\AtendimentoController@atendimentosRelatorio')->name('atendimentosRelatorio');
    Route::post('guiaPDF','Admin\HorarioController@guiaPDF')->name('guiaPDF');
    

});
Auth::routes();

